(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _blocks_blocks_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./blocks/blocks.component */ "./src/app/blocks/blocks.component.ts");
/* harmony import */ var _exercises_list_exercises_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./exercises-list/exercises-list.component */ "./src/app/exercises-list/exercises-list.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _students_list_students_list_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./students-list/students-list.component */ "./src/app/students-list/students-list.component.ts");
/* harmony import */ var _ranking_ranking_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ranking/ranking.component */ "./src/app/ranking/ranking.component.ts");
/* harmony import */ var _exercise_form_exercise_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./exercise-form/exercise-form.component */ "./src/app/exercise-form/exercise-form.component.ts");










var routes = [
    { path: '', component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'blocks', component: _blocks_blocks_component__WEBPACK_IMPORTED_MODULE_4__["BlocksComponent"] },
    { path: 'exercises/list', component: _exercises_list_exercises_list_component__WEBPACK_IMPORTED_MODULE_5__["ExercisesListComponent"] },
    { path: 'register', component: _register_register_component__WEBPACK_IMPORTED_MODULE_6__["RegisterComponent"] },
    { path: 'students', component: _students_list_students_list_component__WEBPACK_IMPORTED_MODULE_7__["StudentsListComponent"] },
    { path: 'ranking', component: _ranking_ranking_component__WEBPACK_IMPORTED_MODULE_8__["RankingComponent"] },
    { path: 'exerciseForm', component: _exercise_form_exercise_form_component__WEBPACK_IMPORTED_MODULE_9__["ExerciseFormComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n    EasyAssembly\n    <button mat-button (click)=\"logOut()\">Log out</button>\n    <button mat-button (click)=\"redirectToExercises()\">Exercises</button>\n    <button mat-button (click)=\"redirectToStudents()\">Students</button>\n    <button mat-button (click)=\"redirectToRanking()\">Ranking</button>\n    <button mat-button (click)=\"redirectToExerciseForm()\">New Exercise</button>\n</mat-toolbar>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AppComponent = /** @class */ (function () {
    function AppComponent(authService, router) {
        this.authService = authService;
        this.router = router;
        this.title = 'easyassembly';
    }
    AppComponent.prototype.logOut = function () {
        this.authService.logout();
    };
    AppComponent.prototype.redirectToExercises = function () {
        this.router.navigateByUrl('exercises/list');
    };
    AppComponent.prototype.redirectToStudents = function () {
        this.router.navigateByUrl('students');
    };
    AppComponent.prototype.redirectToRanking = function () {
        this.router.navigateByUrl('ranking');
    };
    AppComponent.prototype.redirectToExerciseForm = function () {
        this.router.navigateByUrl('exerciseForm');
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _blocks_blocks_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./blocks/blocks.component */ "./src/app/blocks/blocks.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/fesm5/angular-fontawesome.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/esm5/button-toggle.es5.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var angular_font_awesome__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! angular-font-awesome */ "./node_modules/angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _auth_interceptor_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./auth-interceptor.service */ "./src/app/auth-interceptor.service.ts");
/* harmony import */ var _exercises_list_exercises_list_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./exercises-list/exercises-list.component */ "./src/app/exercises-list/exercises-list.component.ts");
/* harmony import */ var _exercises_exercises_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./exercises/exercises.component */ "./src/app/exercises/exercises.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _students_list_students_list_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./students-list/students-list.component */ "./src/app/students-list/students-list.component.ts");
/* harmony import */ var _ranking_ranking_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./ranking/ranking.component */ "./src/app/ranking/ranking.component.ts");
/* harmony import */ var _exercise_form_exercise_form_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./exercise-form/exercise-form.component */ "./src/app/exercise-form/exercise-form.component.ts");






















// Interceptors







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _blocks_blocks_component__WEBPACK_IMPORTED_MODULE_6__["BlocksComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_21__["LoginComponent"],
                _exercises_list_exercises_list_component__WEBPACK_IMPORTED_MODULE_23__["ExercisesListComponent"],
                _exercises_exercises_component__WEBPACK_IMPORTED_MODULE_24__["ExercisesComponent"],
                _register_register_component__WEBPACK_IMPORTED_MODULE_25__["RegisterComponent"],
                _students_list_students_list_component__WEBPACK_IMPORTED_MODULE_26__["StudentsListComponent"],
                _ranking_ranking_component__WEBPACK_IMPORTED_MODULE_27__["RankingComponent"],
                _exercise_form_exercise_form_component__WEBPACK_IMPORTED_MODULE_28__["ExerciseFormComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_9__["DragDropModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__["MatFormFieldModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"],
                _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
                _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_14__["MatToolbarModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_15__["FlexLayoutModule"],
                _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_19__["ScrollingModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_16__["MatTooltipModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_20__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_17__["HttpClientModule"],
                angular_font_awesome__WEBPACK_IMPORTED_MODULE_18__["AngularFontAwesomeModule"]
            ],
            providers: [
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_17__["HTTP_INTERCEPTORS"],
                    useClass: _auth_interceptor_service__WEBPACK_IMPORTED_MODULE_22__["AuthInterceptorService"],
                    multi: true
                }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth-interceptor.service.ts":
/*!*********************************************!*\
  !*** ./src/app/auth-interceptor.service.ts ***!
  \*********************************************/
/*! exports provided: AuthInterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptorService", function() { return AuthInterceptorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var AuthInterceptorService = /** @class */ (function () {
    function AuthInterceptorService(router) {
        this.router = router;
    }
    AuthInterceptorService.prototype.intercept = function (req, next) {
        var _this = this;
        var re = 'token';
        var request = req;
        if (req.url.search(re) === -1) {
            var token = localStorage.getItem('token');
            token += ':undefined';
            token = btoa(token);
            if (token) {
                request = req.clone({
                    setHeaders: {
                        authorization: "Basic " + token
                    }
                });
            }
        }
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (err) {
            if (err.status === 401) {
                _this.router.navigateByUrl('/login');
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(err);
        }));
    };
    AuthInterceptorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], AuthInterceptorService);
    return AuthInterceptorService;
}());



/***/ }),

/***/ "./src/app/auth.service.ts":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");





var AuthService = /** @class */ (function () {
    function AuthService(http, router) {
        this.http = http;
        this.router = router;
        console.log(src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrl);
    }
    AuthService.prototype.login = function (email, password) {
        var _this = this;
        var user = email + ':' + password;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ Authorization: 'Basic ' + btoa(user) });
        this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrl + '/api/token', { headers: headers }).subscribe(function (token) {
            localStorage.setItem('token', token.token);
            _this.router.navigateByUrl('exercises/list');
        });
    };
    AuthService.prototype.getResource = function () {
        this.http.get('/api/resource').subscribe(function (resource) {
            console.log(resource);
        });
    };
    AuthService.prototype.setSession = function (authResult) {
        localStorage.setItem('token', authResult.idToken);
    };
    AuthService.prototype.logout = function () {
        localStorage.removeItem('token');
        this.router.navigateByUrl('/login');
    };
    AuthService.prototype.signUp = function (email, password, name) {
        this.http.post('api/api/user', {
            username: email,
            password: password,
            name: name,
            professor: false
        }).subscribe();
        console.log('ok!');
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/blocks/blocks.component.html":
/*!**********************************************!*\
  !*** ./src/app/blocks/blocks.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n  <fa-icon [icon]=\"faMicrochip\"></fa-icon> EasyAssembly\n</mat-toolbar>\n<div class=\"content\" fxLayout=\"row\">\n  <div cdkDropListGroup class=\"blocks\" fxFlex=\"50\" fxLayout=\"row\" fxLayoutAlign=\"space-around start\">\n    <div class=\"drag-container\">\n      <h2>Instructions</h2>\n\n      <div cdkDropList [cdkDropListData]=\"instructions\" class=\"instructions-list\">\n        <div class=\"block-box\" *ngFor=\"let item of instructions\" cdkDrag>\n          {{item.name}}\n          <div *ngIf=\"item.name === 'ADD'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Op1\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Op2\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Destination\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'MOVE'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Source\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Destination\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'COMPARE'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"R0\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"R1\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'IN'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Port\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Destination\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'OUT'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Source\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Port\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'JUMP'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Tag\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'JNE'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Tag\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'JE'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Tag\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'CALL'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput disabled placeholder=\"Funcion\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'DATA'\">\n              <form class=\"instruction-form\">\n                <mat-form-field class=\"instruction-input\">\n                  <input matInput disabled placeholder=\"Content\">\n                </mat-form-field>\n              </form>\n            </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"drag-container\">\n      <h2>Memory</h2>\n      <div cdkDropList [cdkDropListData]=\"memory\" class=\"memory-list\" (cdkDropListDropped)=\"drop($event)\">\n        <div class=\"block-box\" *ngFor=\"let item of memory\" [attr.index]=\"i\" cdkDrag>\n          <div class=\"custom-placeholder\" *cdkDragPlaceholder></div>\n          {{item.index}} - {{item.name}}\n          <div *ngIf=\"item.name === 'ADD'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Op1\" [(ngModel)]=\"item.op1\" name=\"{{i}}_op1\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Op2\" [(ngModel)]=\"item.op2\" name=\"{{i}}_op2\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Destination\" [(ngModel)]=\"item.res\" name=\"{{i}}_res\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'MOVE'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Source\" [(ngModel)]=\"item.op1\" name=\"{{i}}_op1\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Destination\" [(ngModel)]=\"item.op2\" name=\"{{i}}_op2\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'COMPARE'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"R0\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"R1\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'IN'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Port\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Destination\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'OUT'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Source\">\n              </mat-form-field>\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Port\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'JUMP'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Tag\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'JNE'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Tag\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'JE'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Tag\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'CALL'\">\n            <form class=\"instruction-form\">\n              <mat-form-field class=\"instruction-input\">\n                <input matInput placeholder=\"Funcion\">\n              </mat-form-field>\n            </form>\n          </div>\n          <div *ngIf=\"item.name === 'DATA'\">\n              <form class=\"instruction-form\">\n                <mat-form-field class=\"instruction-input\">\n                  <input matInput placeholder=\"Content\" [(ngModel)]=\"item.op1\" name=\"{{i}}_op1\">\n                </mat-form-field>\n              </form>\n            </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"registers\" fxFlex=\"50\" fxLayout=\"column\" fxLayoutAlign=\"start none\">\n    <mat-card class=\"CPU\">\n      <div>\n        <mat-card-title>CPU</mat-card-title>\n        <div fxLayout=\"row\">\n          <p class=\"aluTitle\" fxFlex=\"50\">ALU</p>\n          <mat-card-content fxFlex=\"50\">\n            <div class=\"registerLabel\"> R0: <div class=\"register\">{{cpu.R0? cpu.R0: '?'}}</div>\n            </div>\n            <div class=\"registerLabel\"> R1: <div class=\"register\">{{cpu.R1? cpu.R1: '?'}}</div>\n            </div>\n            <div class=\"registerLabel\"> R2: <div class=\"register\">{{cpu.R2? cpu.R2: '?'}}</div>\n            </div>\n          </mat-card-content>\n        </div>\n        <mat-card class=\"CPU CU\">\n          <mat-card-title>CU</mat-card-title>\n          <mat-card-content>\n            <div class=\"registerLabel\">PC: <div class=\"register\">{{cpu.PC}}</div>\n            </div>\n            <div class=\"registerLabel\">IR: <div class=\"register\">{{cpu.IR? cpu.IR.name: '?'}}</div>\n            </div>\n          </mat-card-content>\n        </mat-card>\n        <mat-card class=\"entradaSalida\">\n          <mat-card-title>Dispositivo E/S</mat-card-title>\n          <mat-card-content>\n            <div class=\"registerLabel\">Puerto de Datos (PD): <div class=\"register\">{{cpu.PD? cpu.PD: '?'}}</div>\n            </div>\n            <div class=\"registerLabel\">Puerto de Estado (PE): <div class=\"register\">{{cpu.PE? cpu.PE: '?'}}</div>\n            </div>\n          </mat-card-content>\n        </mat-card>\n      </div>\n    </mat-card>\n    <mat-card class=\"buttons\" fxLayout=\"row\" fxLayoutAlign=\"space-evenly center\">\n      <button mat-raised-button (click)=\"stopExecution()\" matTooltip=\"Stop the execution\">\n        <fa-icon [icon]=\"faStop\" ></fa-icon>\n      </button>\n      <button mat-raised-button (click)=\"startExecution()\" [disabled]=\"executing\"\n        matTooltip=\"Start the execution of the program\">\n        <fa-icon [icon]=\"faPlay\"></fa-icon>\n      </button>\n      <button mat-raised-button matTooltip=\"Execute next execution\" (click)=\"executeInstruction()\">\n        <fa-icon [icon]=\"faStepForward\"></fa-icon>\n      </button>\n    </mat-card>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/blocks/blocks.component.scss":
/*!**********************************************!*\
  !*** ./src/app/blocks/blocks.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".drag-container {\n  width: 400px;\n  max-width: 100%;\n  margin: 0 25px 25px 0;\n  display: inline-block;\n  vertical-align: top; }\n\n.instructions-list {\n  border: solid 1px #ccc;\n  min-height: 60px;\n  background: white;\n  border-radius: 4px;\n  overflow: hidden;\n  display: block; }\n\n.memory-list {\n  border: dotted 3px #ccc;\n  min-height: 60px;\n  background: #f2f2f2;\n  border-radius: 4px;\n  overflow: hidden;\n  display: block; }\n\n.block-box {\n  padding: 20px 10px;\n  border-bottom: solid 1px #ccc;\n  color: rgba(0, 0, 0, 0.87);\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: space-between;\n  box-sizing: border-box;\n  cursor: move;\n  background: #e6e6ff;\n  font-size: 14px; }\n\n.cdk-drag-preview {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12); }\n\n.cdk-drag-placeholder {\n  opacity: 0; }\n\n.cdk-drag-animating {\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1); }\n\n.block-box:last-child {\n  border: none; }\n\n.instructions-list.cdk-drop-list-dragging .block-box:not(.cdk-drag-placeholder) {\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1); }\n\n.custom-placeholder {\n  background: #ccc;\n  border: dotted 3px #999;\n  min-height: 60px;\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1); }\n\n.registers {\n  width: 50%; }\n\n.CU {\n  background-color: #ffcc99 !important; }\n\n.CPU {\n  text-align: center;\n  background-color: #99ccff; }\n\n.CPU .aluTitle {\n    font-size: 42px;\n    margin: auto; }\n\n.CPU .registerLabel {\n    border: 1px solid none;\n    margin: 20px; }\n\n.CPU .register {\n    border: 1px solid grey;\n    text-align: center;\n    width: 200px;\n    display: inline-block;\n    border-radius: 5px;\n    background-color: #f2f2f2; }\n\n.CPU mat-card {\n    margin: 10px; }\n\n.CPU mat-card-content {\n    font-size: 28px; }\n\n.content {\n  padding: 10px;\n  display: flex; }\n\n.blocks {\n  max-width: 50%; }\n\n.instruction-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n  display: flex; }\n\n.instruction-input {\n  width: 100px;\n  margin-left: 10px; }\n\n.buttons {\n  margin-top: 20px;\n  background-color: #f2f2f2; }\n\n.buttons button {\n  margin: 10px;\n  height: 100px;\n  width: 100px; }\n\n.entradaSalida {\n  background-color: #e6ff00; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2FuZ2VscG9sby9Fc2NyaXRvcmlvL0dpdGxhYi90Zmcvc3JjL2FwcC9ibG9ja3MvYmxvY2tzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZixxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLHNCQUFzQjtFQUN0QixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQUdoQjtFQUNFLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQUdoQjtFQUNFLGtCQUFrQjtFQUNsQiw2QkFBNkI7RUFDN0IsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixzQkFBc0I7RUFDdEIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixlQUFlLEVBQUE7O0FBR2pCO0VBQ0Usc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixxSEFBcUgsRUFBQTs7QUFHdkg7RUFDRSxVQUFVLEVBQUE7O0FBR1o7RUFDRSxzREFBc0QsRUFBQTs7QUFHeEQ7RUFDRSxZQUFZLEVBQUE7O0FBSWQ7RUFDRSxzREFBc0QsRUFBQTs7QUFHeEQ7RUFDRSxnQkFBZ0I7RUFDaEIsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixzREFBc0QsRUFBQTs7QUFHeEQ7RUFDRSxVQUFVLEVBQUE7O0FBR1o7RUFDRSxvQ0FBb0MsRUFBQTs7QUFHdEM7RUFDRSxrQkFBa0I7RUFDbEIseUJBQXlCLEVBQUE7O0FBRjNCO0lBSUksZUFBZTtJQUNmLFlBQVksRUFBQTs7QUFMaEI7SUFTSSxzQkFBc0I7SUFDdEIsWUFBWSxFQUFBOztBQVZoQjtJQWFJLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLHFCQUFxQjtJQUNyQixrQkFBa0I7SUFDbEIseUJBQXlCLEVBQUE7O0FBbEI3QjtJQXNCSSxZQUFZLEVBQUE7O0FBdEJoQjtJQTBCSSxlQUFlLEVBQUE7O0FBSW5CO0VBQ0UsYUFBYTtFQUNiLGFBQWEsRUFBQTs7QUFHZjtFQUNFLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxhQUFhLEVBQUE7O0FBR2Y7RUFDRSxZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0UsZ0JBQWdCO0VBQ2hCLHlCQUF5QixFQUFBOztBQUczQjtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWSxFQUFBOztBQUdkO0VBQ0UseUJBQXlCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9ibG9ja3MvYmxvY2tzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRyYWctY29udGFpbmVyIHtcbiAgd2lkdGg6IDQwMHB4O1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMCAyNXB4IDI1cHggMDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xufVxuXG4uaW5zdHJ1Y3Rpb25zLWxpc3Qge1xuICBib3JkZXI6IHNvbGlkIDFweCAjY2NjO1xuICBtaW4taGVpZ2h0OiA2MHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLm1lbW9yeS1saXN0IHtcbiAgYm9yZGVyOiBkb3R0ZWQgM3B4ICNjY2M7XG4gIG1pbi1oZWlnaHQ6IDYwcHg7XG4gIGJhY2tncm91bmQ6ICNmMmYyZjI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5ibG9jay1ib3gge1xuICBwYWRkaW5nOiAyMHB4IDEwcHg7XG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjY2NjO1xuICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjg3KTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBjdXJzb3I6IG1vdmU7XG4gIGJhY2tncm91bmQ6ICNlNmU2ZmY7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLmNkay1kcmFnLXByZXZpZXcge1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJveC1zaGFkb3c6IDAgNXB4IDVweCAtM3B4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA4cHggMTBweCAxcHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMTRweCAycHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn1cblxuLmNkay1kcmFnLXBsYWNlaG9sZGVyIHtcbiAgb3BhY2l0eTogMDtcbn1cblxuLmNkay1kcmFnLWFuaW1hdGluZyB7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAyNTBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcbn1cblxuLmJsb2NrLWJveDpsYXN0LWNoaWxkIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4vL1BsYWNlaG9sZGVyXG4uaW5zdHJ1Y3Rpb25zLWxpc3QuY2RrLWRyb3AtbGlzdC1kcmFnZ2luZyAuYmxvY2stYm94Om5vdCguY2RrLWRyYWctcGxhY2Vob2xkZXIpIHtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDI1MG1zIGN1YmljLWJlemllcigwLCAwLCAwLjIsIDEpO1xufVxuXG4uY3VzdG9tLXBsYWNlaG9sZGVyIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyOiBkb3R0ZWQgM3B4ICM5OTk7XG4gIG1pbi1oZWlnaHQ6IDYwcHg7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAyNTBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcbn1cblxuLnJlZ2lzdGVycyB7XG4gIHdpZHRoOiA1MCU7XG59XG5cbi5DVSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNjOTkgIWltcG9ydGFudDtcbn1cblxuLkNQVSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzk5Y2NmZjtcbiAgLmFsdVRpdGxlIHtcbiAgICBmb250LXNpemU6IDQycHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICB9XG5cbiAgLnJlZ2lzdGVyTGFiZWwge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIG5vbmU7XG4gICAgbWFyZ2luOiAyMHB4O1xuICB9XG4gIC5yZWdpc3RlciB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgZ3JleTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDIwMHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcbiAgfVxuXG4gIG1hdC1jYXJkIHtcbiAgICBtYXJnaW46IDEwcHg7XG4gIH1cblxuICBtYXQtY2FyZC1jb250ZW50IHtcbiAgICBmb250LXNpemU6IDI4cHg7XG4gIH1cbn1cblxuLmNvbnRlbnQge1xuICBwYWRkaW5nOiAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uYmxvY2tzIHtcbiAgbWF4LXdpZHRoOiA1MCU7XG59XG5cbi5pbnN0cnVjdGlvbi1mb3JtIHtcbiAgbWluLXdpZHRoOiAxNTBweDtcbiAgbWF4LXdpZHRoOiA1MDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5pbnN0cnVjdGlvbi1pbnB1dCB7XG4gIHdpZHRoOiAxMDBweDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5idXR0b25zIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcbn1cblxuLmJ1dHRvbnMgYnV0dG9uIHtcbiAgbWFyZ2luOiAxMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG59XG5cbi5lbnRyYWRhU2FsaWRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2ZmYwMDtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/blocks/blocks.component.ts":
/*!********************************************!*\
  !*** ./src/app/blocks/blocks.component.ts ***!
  \********************************************/
/*! exports provided: BlocksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlocksComponent", function() { return BlocksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _instruction_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../instruction.model */ "./src/app/instruction.model.ts");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _cpu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../cpu */ "./src/app/cpu.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");







var BlocksComponent = /** @class */ (function () {
    function BlocksComponent(authService) {
        this.authService = authService;
        /**
         * Memory array
         */
        this.memory = [];
        /**
         * Registers variables
         */
        /*R0: number = undefined;
        R1: number = undefined;
        R2: number = undefined;
        PC = 0;
        IR: Instruction;
        PD: number;
        PE: number;*/
        this.cpu = new _cpu__WEBPACK_IMPORTED_MODULE_5__["CPU"]();
        /**
         * Check if the instructions are being executed
         */
        this.executing = false;
        /**
         * Icons variables
         */
        this.faPlay = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__["faPlay"];
        this.faStepForward = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__["faStepForward"];
        this.faStepBackward = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__["faStepBackward"];
        this.faStop = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__["faStop"];
        this.faMicrochip = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__["faMicrochip"];
        this.restartInstructions();
    }
    BlocksComponent.prototype.ngOnInit = function () {
        this.authService.getResource();
    };
    /**
     * Method to drag and drop instructions to the memory
     */
    BlocksComponent.prototype.drop = function (event) {
        if (event.previousContainer === event.container) {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_2__["moveItemInArray"])(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_2__["transferArrayItem"])(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.restartInstructions();
            this.memory[event.currentIndex].index = event.currentIndex;
        }
        this.setIndexes();
    };
    /**
     * Set the index of every instruction in memory after another is being added
     */
    BlocksComponent.prototype.setIndexes = function () {
        for (var i = 0; i < this.memory.length; i++) {
            this.memory[i].index = i;
        }
    };
    BlocksComponent.prototype.restartInstructions = function () {
        this.instructions = [
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["MOVE"](),
            // new Instruction('MOVE'),
            //new Instruction('ADD'),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["ADD"](),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["Instruction"]('DATA'),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["Instruction"]('COMPARE'),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["Instruction"]('IN'),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["Instruction"]('OUT'),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["Instruction"]('JUMP'),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["Instruction"]('JNE'),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["Instruction"]('JE'),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["Instruction"]('CALL'),
            new _instruction_model__WEBPACK_IMPORTED_MODULE_3__["Instruction"]('HALT')
        ];
    };
    BlocksComponent.prototype.startExecution = function () {
        this.cpu.R0 = 2;
        this.cpu.R1 = 2;
        this.cpu.R2 = 2;
        this.executing = true;
        this.updateCU();
        //this.executeInstruction();
    };
    BlocksComponent.prototype.stopExecution = function () {
        this.executing = false;
        this.cpu.IR = undefined;
        this.cpu.PC = 0;
    };
    BlocksComponent.prototype.updateCU = function () {
        this.cpu.IR = this.memory[this.cpu.PC];
        this.cpu.PC++;
    };
    BlocksComponent.prototype.executeInstruction = function () {
        var resultado = this.cpu.IR.execute(this.cpu, this.memory);
        if (resultado.error === 'OK') {
            this.cpu = resultado.cpu;
            this.memory = resultado.memory;
            this.updateCU();
        }
        else {
            console.log(resultado.error);
        }
    };
    BlocksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-blocks',
            template: __webpack_require__(/*! ./blocks.component.html */ "./src/app/blocks/blocks.component.html"),
            styles: [__webpack_require__(/*! ./blocks.component.scss */ "./src/app/blocks/blocks.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], BlocksComponent);
    return BlocksComponent;
}());



/***/ }),

/***/ "./src/app/cpu.ts":
/*!************************!*\
  !*** ./src/app/cpu.ts ***!
  \************************/
/*! exports provided: CPU */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CPU", function() { return CPU; });
var CPU = /** @class */ (function () {
    function CPU() {
        /**
         * Registers variables
         */
        this.R0 = undefined;
        this.R1 = undefined;
        this.R2 = undefined;
        this.PC = 0;
        this.R0 = undefined;
        this.R1 = undefined;
        this.R2 = undefined;
        this.PC = 0;
    }
    return CPU;
}());



/***/ }),

/***/ "./src/app/exercise-form/exercise-form.component.html":
/*!************************************************************!*\
  !*** ./src/app/exercise-form/exercise-form.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"card\">\n  <mat-card-header>\n    <mat-card-title>New exercise</mat-card-title>\n  </mat-card-header>\n  <form [formGroup]=\"form\">\n    <div class=\"form-field\">\n      <mat-form-field>\n        <input matInput placeholder=\"Title\" name=\"title\" formControlName=\"title\">\n      </mat-form-field>\n    </div>\n    <div class=\"form-field\">\n      <mat-form-field>\n        <textarea matInput placeholder=\"Description\" name=\"description\" formControlName=\"description\"></textarea>\n      </mat-form-field>\n    </div>\n    <div class=\"form-field\">\n      <mat-form-field>\n        <input matInput placeholder=\"Difficulty\" name=\"difficulty\" formControlName=\"difficulty\">\n      </mat-form-field>\n    </div>\n    <div class=\"form-field\">\n      <mat-form-field>\n        <input matInput placeholder=\"Points\" name=\"points\" formControlName=\"points\">\n      </mat-form-field>\n    </div>\n    <mat-card class=\"result\">\n      <mat-card-header>\n        <mat-card-title>Result</mat-card-title>\n      </mat-card-header>\n      <div>\n        <div class=\"form-field\">\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"R0\" name=\"R0\" formControlName=\"R0\">\n          </mat-form-field>\n        </div>\n        <div class=\"form-field\">\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"R1\" name=\"R1\" formControlName=\"R1\">\n          </mat-form-field>\n        </div>\n        <div class=\"form-field\">\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"R2\" name=\"R2\" formControlName=\"R2\">\n          </mat-form-field>\n        </div>\n      </div>\n      <div>\n      <div class=\"form-field\">\n        <mat-form-field class=\"example-full-width\">\n          <input matInput placeholder=\"PD\" name=\"PD\" formControlName=\"PD\">\n        </mat-form-field>\n      </div>\n      <div class=\"form-field\">\n        <mat-form-field class=\"example-full-width\">\n          <input matInput placeholder=\"PE\" name=\"PE\" formControlName=\"PE\">\n        </mat-form-field>\n      </div>\n    </div>\n    </mat-card>\n    <div class=\"form-buttons\">\n      <button mat-raised-button (click)=\"newExercise()\">Save <i class=\"fa fa-save\"></i></button>\n    </div>\n  </form>\n</mat-card>"

/***/ }),

/***/ "./src/app/exercise-form/exercise-form.component.scss":
/*!************************************************************!*\
  !*** ./src/app/exercise-form/exercise-form.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card {\n  margin: auto;\n  margin-top: 64px;\n  width: 50%; }\n\nmat-form-field {\n  width: 100%; }\n\n.result {\n  display: flex; }\n\n.example-full-width {\n  width: 50%;\n  margin: auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2FuZ2VscG9sby9Fc2NyaXRvcmlvL0dpdGxhYi90Zmcvc3JjL2FwcC9leGVyY2lzZS1mb3JtL2V4ZXJjaXNlLWZvcm0uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLFVBQVUsRUFBQTs7QUFHWjtFQUNFLFdBQVcsRUFBQTs7QUFHYjtFQUNFLGFBQWEsRUFBQTs7QUFHZjtFQUNFLFVBQVU7RUFDVixZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9leGVyY2lzZS1mb3JtL2V4ZXJjaXNlLWZvcm0uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZCB7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLXRvcDogNjRweDtcbiAgd2lkdGg6IDUwJTtcbn1cblxubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLnJlc3VsdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5leGFtcGxlLWZ1bGwtd2lkdGgge1xuICB3aWR0aDogNTAlO1xuICBtYXJnaW46IGF1dG87XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/exercise-form/exercise-form.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/exercise-form/exercise-form.component.ts ***!
  \**********************************************************/
/*! exports provided: ExerciseFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExerciseFormComponent", function() { return ExerciseFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _exercises_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../exercises.service */ "./src/app/exercises.service.ts");




var ExerciseFormComponent = /** @class */ (function () {
    function ExerciseFormComponent(exercisesService, fb) {
        this.exercisesService = exercisesService;
        this.fb = fb;
        this.form = this.fb.group({
            title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            difficulty: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            points: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            R0: [''],
            R1: [''],
            R2: [''],
            PD: [''],
            PE: [''],
        });
    }
    ExerciseFormComponent.prototype.ngOnInit = function () {
    };
    ExerciseFormComponent.prototype.newExercise = function () {
        var form = this.form.value;
        var result = {
            title: form.title,
            description: form.description,
            difficulty: form.difficulty,
            points: form.points,
            result: {
                R0: form.R0,
                R1: form.R1,
                R2: form.R2
            }
        };
        this.exercisesService.newExercise(result).subscribe(function (data) {
            console.log(data);
        });
    };
    ExerciseFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-exercise-form',
            template: __webpack_require__(/*! ./exercise-form.component.html */ "./src/app/exercise-form/exercise-form.component.html"),
            styles: [__webpack_require__(/*! ./exercise-form.component.scss */ "./src/app/exercise-form/exercise-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_exercises_service__WEBPACK_IMPORTED_MODULE_3__["ExercisesService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], ExerciseFormComponent);
    return ExerciseFormComponent;
}());



/***/ }),

/***/ "./src/app/exercises-list/exercises-list.component.html":
/*!**************************************************************!*\
  !*** ./src/app/exercises-list/exercises-list.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"lista-ejercicios\">\n  <mat-card-header>\n    <mat-card-title>List of exercises</mat-card-title>\n  </mat-card-header>\n  <mat-card *ngFor=\"let exercise of exercises\" class=\"ejercicio\" routerLink=\"path\">\n    <p>{{exercise.title}}</p>\n    <div class=\"difficulty\">\n      Dificultad:  \n      <div *ngFor='let in of counter(exercise.difficulty) ;let i = index'> <i class=\"fa fa-star\"></i></div>\n    </div>\n  </mat-card>\n</mat-card>"

/***/ }),

/***/ "./src/app/exercises-list/exercises-list.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/exercises-list/exercises-list.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".lista-ejercicios {\n  border: 3px solid  #3f51b5;\n  width: 50%;\n  margin: auto;\n  margin-top: 20px; }\n\n.ejercicio {\n  margin: 20px;\n  border: 3px solid #3f51b5; }\n\n.ejercicio:hover {\n  cursor: pointer; }\n\n.difficulty {\n  display: flex; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2FuZ2VscG9sby9Fc2NyaXRvcmlvL0dpdGxhYi90Zmcvc3JjL2FwcC9leGVyY2lzZXMtbGlzdC9leGVyY2lzZXMtbGlzdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDBCQUEwQjtFQUMxQixVQUFVO0VBQ1YsWUFBWTtFQUNaLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLFlBQVk7RUFDWix5QkFBeUIsRUFBQTs7QUFHM0I7RUFFRSxlQUFlLEVBQUE7O0FBR2pCO0VBQ0UsYUFBYSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvZXhlcmNpc2VzLWxpc3QvZXhlcmNpc2VzLWxpc3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGlzdGEtZWplcmNpY2lvcyB7XG4gIGJvcmRlcjogM3B4IHNvbGlkICAjM2Y1MWI1O1xuICB3aWR0aDogNTAlO1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi5lamVyY2ljaW8ge1xuICBtYXJnaW46IDIwcHg7XG4gIGJvcmRlcjogM3B4IHNvbGlkICMzZjUxYjU7XG59XG5cbi5lamVyY2ljaW86aG92ZXIge1xuICAvL2JhY2tncm91bmQtY29sb3I6ICMzZjUxYjU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmRpZmZpY3VsdHkge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/exercises-list/exercises-list.component.ts":
/*!************************************************************!*\
  !*** ./src/app/exercises-list/exercises-list.component.ts ***!
  \************************************************************/
/*! exports provided: ExercisesListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExercisesListComponent", function() { return ExercisesListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _exercises_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../exercises.service */ "./src/app/exercises.service.ts");



var ExercisesListComponent = /** @class */ (function () {
    function ExercisesListComponent(exercisesService) {
        this.exercisesService = exercisesService;
    }
    ExercisesListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.exercisesService.list().subscribe(function (list) {
            _this.exercises = list;
            console.log(_this.exercises);
        });
    };
    ExercisesListComponent.prototype.counter = function (i) {
        return new Array(i);
    };
    ExercisesListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-exercises-list',
            template: __webpack_require__(/*! ./exercises-list.component.html */ "./src/app/exercises-list/exercises-list.component.html"),
            styles: [__webpack_require__(/*! ./exercises-list.component.scss */ "./src/app/exercises-list/exercises-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_exercises_service__WEBPACK_IMPORTED_MODULE_2__["ExercisesService"]])
    ], ExercisesListComponent);
    return ExercisesListComponent;
}());



/***/ }),

/***/ "./src/app/exercises.service.ts":
/*!**************************************!*\
  !*** ./src/app/exercises.service.ts ***!
  \**************************************/
/*! exports provided: ExercisesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExercisesService", function() { return ExercisesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var ExercisesService = /** @class */ (function () {
    function ExercisesService(http) {
        this.http = http;
    }
    ExercisesService.prototype.list = function () {
        return this.http.get('/api/api/exercises');
    };
    ExercisesService.prototype.newExercise = function (exercise) {
        console.log(exercise);
        return this.http.post('api/api/exercise', {
            title: exercise.title,
            description: exercise.description,
            difficulty: exercise.difficulty,
            points: exercise.points,
            result: JSON.stringify(exercise.result)
        });
    };
    ExercisesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ExercisesService);
    return ExercisesService;
}());



/***/ }),

/***/ "./src/app/exercises/exercises.component.html":
/*!****************************************************!*\
  !*** ./src/app/exercises/exercises.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  exercises works!\n</p>\n"

/***/ }),

/***/ "./src/app/exercises/exercises.component.scss":
/*!****************************************************!*\
  !*** ./src/app/exercises/exercises.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4ZXJjaXNlcy9leGVyY2lzZXMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/exercises/exercises.component.ts":
/*!**************************************************!*\
  !*** ./src/app/exercises/exercises.component.ts ***!
  \**************************************************/
/*! exports provided: ExercisesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExercisesComponent", function() { return ExercisesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ExercisesComponent = /** @class */ (function () {
    function ExercisesComponent() {
    }
    ExercisesComponent.prototype.ngOnInit = function () {
    };
    ExercisesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-exercises',
            template: __webpack_require__(/*! ./exercises.component.html */ "./src/app/exercises/exercises.component.html"),
            styles: [__webpack_require__(/*! ./exercises.component.scss */ "./src/app/exercises/exercises.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ExercisesComponent);
    return ExercisesComponent;
}());



/***/ }),

/***/ "./src/app/instruction.model.ts":
/*!**************************************!*\
  !*** ./src/app/instruction.model.ts ***!
  \**************************************/
/*! exports provided: Instruction, MOVE, ADD */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Instruction", function() { return Instruction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MOVE", function() { return MOVE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD", function() { return ADD; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var Instruction = /** @class */ (function () {
    function Instruction(name) {
        this.executing = false;
        this.name = name;
    }
    Instruction.prototype.execute = function (cpu, memory) {
        return { error: 'FUNCIONALIDAD NO IMPLEMENTADA!' };
    };
    Instruction.prototype.hasRegister = function () {
        if (this.op1 === 'R0' || this.op1 === 'R1' || this.op1 === 'R2' ||
            this.op2 === 'R0' || this.op2 === 'R1' || this.op2 === 'R2') {
            return true;
        }
        else {
            return false;
        }
    };
    Instruction.prototype.allAreRegisters = function () {
        if (this.op1 === 'R0' || this.op1 === 'R1' || this.op1 === 'R2' ||
            this.op2 === 'R0' || this.op2 === 'R1' || this.op2 === 'R2' ||
            this.res === 'R0' || this.res === 'R1' || this.res === 'R2') {
            return true;
        }
        else {
            return false;
        }
    };
    return Instruction;
}());

var MOVE = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](MOVE, _super);
    function MOVE() {
        return _super.call(this, 'MOVE') || this;
    }
    MOVE.prototype.execute = function (cpu, memory) {
        if (cpu.IR.op1.includes('R')) {
            if (cpu.IR.op1 === 'R0') {
                memory[+cpu.IR.op2].op1 = cpu.R0;
            }
            else if (cpu.IR.op1 === 'R1') {
                memory[+cpu.IR.op2].op1 = cpu.R1;
            }
            else if (cpu.IR.op1 === 'R2') {
                memory[+cpu.IR.op2].op1 = cpu.R2;
            }
        }
        else if (cpu.IR.op2.includes('R')) {
            if (cpu.IR.op2 === 'R1') {
                cpu.R1 = memory[+cpu.IR.op1].op1;
            }
            else if (cpu.IR.op2 === 'R2') {
                cpu.R2 = memory[+cpu.IR.op1].op1;
            }
            else if (cpu.IR.op2 === 'R0') {
                cpu.R0 = memory[+cpu.IR.op1].op1;
            }
        }
        else {
            return { error: 'Al menos uno de los operandos debe ser un registro' };
        }
        return { cpu: cpu, memory: memory, error: 'OK' };
    };
    return MOVE;
}(Instruction));

var ADD = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ADD, _super);
    function ADD() {
        return _super.call(this, 'ADD') || this;
    }
    ADD.prototype.execute = function (cpu, memory) {
        if (this.allAreRegisters()) {
            if ((this.op1 === 'R0' && this.op2 === 'R1' && this.res === 'R2') ||
                (this.op1 === 'R1' && this.op2 === 'R0' && this.res === 'R2')) {
                console.log('r2=r1+r0');
                cpu.R2 = cpu.R0 + cpu.R1;
            }
            else if ((this.op1 === 'R0' && this.op2 === 'R2' && this.res === 'R1') ||
                (this.op1 === 'R2' && this.op2 === 'R0' && this.res === 'R1')) {
                console.log('r1=r2+r0');
                cpu.R1 = cpu.R0 + cpu.R2;
            }
            else if ((this.op1 === 'R1' && this.op2 === 'R2' && this.res === 'R0') ||
                (this.op1 === 'R2' && this.op2 === 'R1' && this.res === 'R0')) {
                console.log('r0=r1+r2');
                cpu.R0 = cpu.R1 + cpu.R2;
            }
            console.log('no ha entrao ' + this.op1 + ' ' + this.op2 + ' ' + this.res);
            console.log('OK ADD');
            return { cpu: cpu, memory: memory, error: 'OK' };
        }
        else {
            return { error: 'Todos los operandos deben ser registros' };
        }
    };
    return ADD;
}(Instruction));



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login\">\n    <mat-card>\n        <form [formGroup]=\"form\">\n            <h1>Easy Assembly</h1>\n            <h2>Log in</h2>\n            <div class=\"form-field\">\n                <mat-form-field class=\"example-full-width\">\n                    <input matInput placeholder=\"Email\" name=\"email\" formControlName=\"email\">\n                </mat-form-field>\n            </div>\n            <div class=\"form-field\">\n                <mat-form-field class=\"example-full-width\">\n                    <input matInput placeholder=\"Password\" name=\"password\" formControlName=\"password\" type=\"password\">\n                </mat-form-field>\n            </div>\n            <div class=\"form-buttons\">\n                <button mat-raised-button (click)=\"login()\">Login</button>\n            </div>\n        </form>\n        <button mat-raised-button (click)=\"redirectToRegister()\">Sign up</button>\n    </mat-card>\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login {\n  align-items: center; }\n\nh2, h1 {\n  text-align: center; }\n\nmat-card {\n  width: 300px;\n  margin: auto;\n  margin-top: 64px;\n  border: 3px solid  #3f51b5; }\n\nmat-form-field {\n  width: 100%; }\n\nbutton {\n  text-align: center;\n  width: 100%;\n  margin: 2px;\n  background-color: #3f51b5;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2FuZ2VscG9sby9Fc2NyaXRvcmlvL0dpdGxhYi90Zmcvc3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdFLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLDBCQUEwQixFQUFBOztBQUc1QjtFQUNFLFdBQVcsRUFBQTs7QUFHYjtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dpbiB7XG4gIC8vZGlzcGxheTogZmxleDtcbiAgLy9qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjsgXG59XG5cbmgyLCBoMSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxubWF0LWNhcmQge1xuICB3aWR0aDogMzAwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLXRvcDogNjRweDtcbiAgYm9yZGVyOiAzcHggc29saWQgICMzZjUxYjU7XG59XG5cbm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmJ1dHRvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2Y1MWI1O1xuICBjb2xvcjogd2hpdGU7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");





var LoginComponent = /** @class */ (function () {
    function LoginComponent(fb, router, authService) {
        this.fb = fb;
        this.router = router;
        this.authService = authService;
        this.form = this.fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    LoginComponent.prototype.login = function () {
        var val = this.form.value;
        if (val.email && val.password) {
            this.authService.login(val.email, val.password);
        }
    };
    LoginComponent.prototype.redirectToRegister = function () {
        this.router.navigateByUrl('/register');
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/ranking/ranking.component.html":
/*!************************************************!*\
  !*** ./src/app/ranking/ranking.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"students-list\">\n  <mat-card-header>\n    <mat-card-title>Ranking</mat-card-title>\n  </mat-card-header>\n  <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\" class=\"labels\">\n    <p>Position</p>\n    <p>Name</p>\n    <p>Points</p>\n  </div>\n  <mat-card *ngFor=\"let student of list; let i=index\" class=\"student\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n      <div class=\"field\">{{i+1}}</div>\n    <div class=\"field\">{{student.name}}</div>\n    <div class=\"field\">{{student.points}}</div>\n  </mat-card>\n</mat-card>"

/***/ }),

/***/ "./src/app/ranking/ranking.component.scss":
/*!************************************************!*\
  !*** ./src/app/ranking/ranking.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".students-list {\n  margin: auto;\n  margin-top: 64px;\n  width: 33%; }\n\n.student {\n  margin: 25px; }\n\n.labels {\n  margin-left: 30px;\n  margin-right: 30px; }\n\nmat-card {\n  border: 3px solid  #3f51b5; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2FuZ2VscG9sby9Fc2NyaXRvcmlvL0dpdGxhYi90Zmcvc3JjL2FwcC9yYW5raW5nL3JhbmtpbmcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLFVBQVUsRUFBQTs7QUFHWjtFQUNFLFlBQVksRUFBQTs7QUFHZDtFQUNFLGlCQUFpQjtFQUNqQixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSwwQkFBMEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3JhbmtpbmcvcmFua2luZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdHVkZW50cy1saXN0IHtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tdG9wOiA2NHB4O1xuICB3aWR0aDogMzMlO1xufVxuXG4uc3R1ZGVudCB7XG4gIG1hcmdpbjogMjVweDtcbn1cblxuLmxhYmVscyB7XG4gIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDMwcHg7XG59XG5cbm1hdC1jYXJkIHtcbiAgYm9yZGVyOiAzcHggc29saWQgICMzZjUxYjU7XG59Il19 */"

/***/ }),

/***/ "./src/app/ranking/ranking.component.ts":
/*!**********************************************!*\
  !*** ./src/app/ranking/ranking.component.ts ***!
  \**********************************************/
/*! exports provided: RankingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RankingComponent", function() { return RankingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _student_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../student.service */ "./src/app/student.service.ts");



var RankingComponent = /** @class */ (function () {
    function RankingComponent(studentService) {
        this.studentService = studentService;
        this.list = [];
    }
    RankingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.studentService.ranking().subscribe(function (ranking) {
            _this.list = ranking['list'];
            console.log(_this.list);
        });
    };
    RankingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ranking',
            template: __webpack_require__(/*! ./ranking.component.html */ "./src/app/ranking/ranking.component.html"),
            styles: [__webpack_require__(/*! ./ranking.component.scss */ "./src/app/ranking/ranking.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_student_service__WEBPACK_IMPORTED_MODULE_2__["StudentService"]])
    ], RankingComponent);
    return RankingComponent;
}());



/***/ }),

/***/ "./src/app/register/register.component.html":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"example-container\">\n  <form [formGroup]=\"form\">\n    <mat-card-header>\n      <mat-card-title>Sign up</mat-card-title>\n    </mat-card-header>\n    <mat-form-field>\n      <input matInput placeholder=\"Email\" type=\"email\" name=\"email\" formControlName=\"email\">\n    </mat-form-field>\n    <mat-form-field>\n      <input matInput placeholder=\"Name\" name=\"name\" formControlName=\"name\">\n    </mat-form-field>\n    <mat-form-field>\n      <input matInput placeholder=\"Password\" type=\"password\" name=\"password\" formControlName=\"password\">\n    </mat-form-field>\n    <button mat-raised-button (click)=\"register()\">Register</button>\n  </form>\n</mat-card>"

/***/ }),

/***/ "./src/app/register/register.component.scss":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column;\n  width: 25%;\n  margin: auto;\n  margin-top: 64px; }\n  .example-container input {\n    width: 75%; }\n  mat-form-field {\n  width: 100%; }\n  button {\n  text-align: center;\n  width: 100%;\n  margin: 2px;\n  background-color: #3f51b5;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2FuZ2VscG9sby9Fc2NyaXRvcmlvL0dpdGxhYi90Zmcvc3JjL2FwcC9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsVUFBVTtFQUNWLFlBQVk7RUFDWixnQkFBZ0IsRUFBQTtFQUxsQjtJQU9JLFVBQVUsRUFBQTtFQUlkO0VBQ0UsV0FBVyxFQUFBO0VBR2I7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB3aWR0aDogMjUlO1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi10b3A6IDY0cHg7XG4gIGlucHV0IHtcbiAgICB3aWR0aDogNzUlO1xuICB9XG59XG5cbm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmJ1dHRvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2Y1MWI1O1xuICBjb2xvcjogd2hpdGU7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(fb, router, authService) {
        this.fb = fb;
        this.router = router;
        this.authService = authService;
        this.form = this.fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.register = function () {
        var val = this.form.value;
        if (val.email && val.password && val.name) {
            this.authService.signUp(val.email, val.password, val.name);
        }
    };
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.scss */ "./src/app/register/register.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/student.service.ts":
/*!************************************!*\
  !*** ./src/app/student.service.ts ***!
  \************************************/
/*! exports provided: StudentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentService", function() { return StudentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var StudentService = /** @class */ (function () {
    function StudentService(http) {
        this.http = http;
    }
    StudentService.prototype.list = function () {
        return this.http.get('/api/users/list');
    };
    StudentService.prototype.editAccepted = function (accepted, email) {
        return this.http.put('/api/users/edit', { email: email, accepted: accepted });
    };
    StudentService.prototype.ranking = function () {
        return this.http.get('api/users/ranking');
    };
    StudentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], StudentService);
    return StudentService;
}());



/***/ }),

/***/ "./src/app/students-list/students-list.component.html":
/*!************************************************************!*\
  !*** ./src/app/students-list/students-list.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"students-list\">\n  <mat-card-header>\n    <mat-card-title>Pending requests</mat-card-title>\n  </mat-card-header>\n  <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\" *ngIf=\"pending.length !== 0\">\n    <p>Name</p>\n    <p>Email</p>\n    <p>Accept/Block</p>\n  </div>\n  <mat-card *ngFor=\"let student of pending\" class=\"student\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n    <p class=\"field\">{{student.email}}</p>\n    <p class=\"field\">{{student.name}}</p>\n    <div class=\"field\" *ngIf=\"student.accepted === null \">\n      <button mat-raised-button (click)=\"editPermission(student, true)\" class=\"accept-button\"><i\n          class=\"fa fa-check\"></i> Accept</button>\n      <button mat-raised-button (click)=\"editPermission(student, false)\" class=\"block-button\"><i class=\"fa fa-ban\"></i>\n        Block</button>\n    </div>\n    <div class=\"field\" *ngIf=\"student.accepted !== null\">\n      <button mat-raised-button disabled class=\"accept-button\"><i class=\"fa fa-check\"></i> Accept</button>\n      <button mat-raised-button disabled class=\"block-button\"><i class=\"fa fa-ban\"></i> Block</button>\n    </div>\n  </mat-card>\n  <p *ngIf=\"pending.length === 0\">There is no pending petitions.</p>\n</mat-card>\n\n<mat-card class=\"students-list\">\n  <mat-card-header>\n    <mat-card-title>Accepted students</mat-card-title>\n  </mat-card-header>\n  <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n    <p>Name</p>\n    <p>Email</p>\n    <p>Accept/Block</p>\n  </div>\n  <mat-card *ngFor=\"let student of accepted\" class=\"student\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n    <p class=\"field\">{{student.email}}</p>\n    <p class=\"field\">{{student.name}}</p>\n    <div class=\"field\" *ngIf=\"student.accepted === true\">\n      <button mat-raised-button (click)=\"editPermission(student, false)\" class=\"block-button\"><i class=\"fa fa-ban\"></i>\n        Block</button>\n    </div>\n    <div class=\"field\" *ngIf=\"student.accepted !== true\">\n      <button mat-raised-button disabled class=\"block-button\">\n        <i class=\"fa fa-ban\"></i>Block\n      </button>\n    </div>\n  </mat-card>\n</mat-card>\n\n<mat-card class=\"students-list\">\n  <mat-card-header>\n    <mat-card-title>Blocked users</mat-card-title>\n  </mat-card-header>\n  <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n    <p>Name</p>\n    <p>Email</p>\n    <p>Accept/Block</p>\n  </div>\n  <mat-card *ngFor=\"let student of declined\" class=\"student\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n    <p class=\"field\">{{student.email}}</p>\n    <p class=\"field\">{{student.name}}</p>\n    <div class=\"field\">\n      <button mat-raised-button (click)=\"editPermission(student, true)\" class=\"accept-button\"><i\n          class=\"fa fa-check\"></i>Accept</button>\n    </div>\n  </mat-card>\n</mat-card>"

/***/ }),

/***/ "./src/app/students-list/students-list.component.scss":
/*!************************************************************!*\
  !*** ./src/app/students-list/students-list.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".students-list {\n  margin: auto;\n  margin-top: 64px;\n  width: 50%; }\n\n.student {\n  margin: 25px; }\n\nbutton {\n  text-align: center;\n  background-color: green;\n  color: white;\n  margin-right: 5px; }\n\n.block-button {\n  background-color: darkred; }\n\nmat-card {\n  border: 3px solid  #3f51b5; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2FuZ2VscG9sby9Fc2NyaXRvcmlvL0dpdGxhYi90Zmcvc3JjL2FwcC9zdHVkZW50cy1saXN0L3N0dWRlbnRzLWxpc3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLFVBQVUsRUFBQTs7QUFHWjtFQUVFLFlBQVksRUFBQTs7QUFRZDtFQUNFLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUduQjtFQUNJLHlCQUF5QixFQUFBOztBQUc3QjtFQUNFLDBCQUEwQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc3R1ZGVudHMtbGlzdC9zdHVkZW50cy1saXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN0dWRlbnRzLWxpc3Qge1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi10b3A6IDY0cHg7XG4gIHdpZHRoOiA1MCU7XG59XG5cbi5zdHVkZW50IHtcbiAgLy93aWR0aDogNTAlO1xuICBtYXJnaW46IDI1cHg7XG4gIC8vZGlzcGxheTogZmxleDtcbn1cblxuLmZpZWxkIHtcbiAgLy9tYXJnaW46IDVweDtcbn1cblxuYnV0dG9uIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmJsb2NrLWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogZGFya3JlZDtcbn1cblxubWF0LWNhcmR7XG4gIGJvcmRlcjogM3B4IHNvbGlkICAjM2Y1MWI1O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/students-list/students-list.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/students-list/students-list.component.ts ***!
  \**********************************************************/
/*! exports provided: StudentsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsListComponent", function() { return StudentsListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _student_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../student.service */ "./src/app/student.service.ts");



var StudentsListComponent = /** @class */ (function () {
    function StudentsListComponent(studentsService) {
        this.studentsService = studentsService;
        this.pending = [];
        this.accepted = [];
        this.declined = [];
    }
    StudentsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.studentsService.list().subscribe(function (list) {
            _this.pending = list['pending'];
            _this.accepted = list['accepted'];
            _this.declined = list['declined'];
        });
    };
    StudentsListComponent.prototype.editPermission = function (email, accepted) {
        this.studentsService.editAccepted(accepted, email.email).subscribe(function (result) {
            console.table(result);
            email.accepted = accepted;
        });
    };
    StudentsListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-students-list',
            template: __webpack_require__(/*! ./students-list.component.html */ "./src/app/students-list/students-list.component.html"),
            styles: [__webpack_require__(/*! ./students-list.component.scss */ "./src/app/students-list/students-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_student_service__WEBPACK_IMPORTED_MODULE_2__["StudentService"]])
    ], StudentsListComponent);
    return StudentsListComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    //apiUrl: 'http://localhost:5000'
    apiUrl: 'https://easy-assembly-api.herokuapp.com'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/angelpolo/Escritorio/Gitlab/tfg/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map
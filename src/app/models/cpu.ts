import {Instruction, MOVE} from './instruction.model';

export class CPU {

  private static instance: CPU;

  /**
   * Registers variables
   */
  R0: number = undefined;
  R1: number = undefined;
  R2: number = undefined;
  PC = 0;
  IR: Instruction;
  PD: number;
  PE: number;

  flag: boolean;

  private constructor() {
    this.R0 = undefined;
    this.R1 = undefined;
    this.R2 = undefined;
    this.PC = 0;

    this.flag = undefined;
  }

  static getInstance() {
    if (!CPU.instance) {
      CPU.instance = new CPU();
    }
    return CPU.instance;
  }

  public reset() {
    this.R0 = undefined;
    this.R1 = undefined;
    this.R2 = undefined;
    this.PC = 0;
    this.IR = undefined;
    this.PD = undefined;
    this.PE = undefined;
    this.flag = undefined;
  }

}

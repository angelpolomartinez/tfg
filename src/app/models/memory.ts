import {Instruction} from './instruction.model';

export class Memory {
  private static instance: Memory;

  public memory: Instruction[];

  private constructor() {
    this.memory = [];
  }

  static getInstance() {
    if (!Memory.instance) {
      Memory.instance = new Memory();
    }
    return Memory.instance;
  }

  public reset() {
    this.memory = [];
  }

}

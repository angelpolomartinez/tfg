import {CPU} from './cpu';
import {Memory} from './memory';

export abstract class Instruction {
  name: string;
  index?: number;
  tag?: string;
  op1?: any;
  op2?: any;
  res?: any;
  executing = false;

  template?;

  execute() {
    return {error: 'OK'};
  }

  hasRegister() {
    if (this.op1 === 'R0' || this.op1 === 'R1' || this.op1 === 'R2' ||
      this.op2 === 'R0' || this.op2 === 'R1' || this.op2 === 'R2') {
      return true;
    } else {
      return false;
    }
  }

  op1IsRegister() {
    if (this.op1 === 'R0' || this.op1 === 'R1' || this.op1 === 'R2') {
      return true;
    } else {
      return false;
    }
  }

  op2IsRegister() {
    if (this.op2 === 'R0' || this.op2 === 'R1' || this.op2 === 'R2') {
      return true;
    } else {
      return false;
    }
  }

  resIsRegister() {
    if (this.res === 'R0' || this.res === 'R1' || this.res === 'R2') {
      return true;
    } else {
      return false;
    }
  }

  allAreRegisters() {
    if (this.op1 === 'R0' || this.op1 === 'R1' || this.op1 === 'R2' ||
      this.op2 === 'R0' || this.op2 === 'R1' || this.op2 === 'R2' ||
      this.res === 'R0' || this.res === 'R1' || this.res === 'R2') {
      return true;
    } else {
      return false;
    }
  }

  bothAreRegisters() {
    if ((this.op1 === 'R0' || this.op1 === 'R1' || this.op1 === 'R2') &&
      (this.op2 === 'R0' || this.op2 === 'R1' || this.op2 === 'R2')) {
      return true;
    } else {
      return false;
    }
  }

  constructor(name: string) {
    this.name = name;
  }
}

export class MOVE extends Instruction {

  constructor() {
    super('MOVE');
  }

  execute() {
    const cpu = CPU.getInstance();
    const memory = Memory.getInstance().memory;

    if (!this.hasRegister() || !cpu.IR.op1 || !cpu.IR.op2) {
      return {error: 'At least one of the operands must be a register.'};
    }

    if ((cpu.IR.op1 as string).includes('R') && memory[+cpu.IR.op2].name === 'DATA') {
      if (cpu.IR.op1 === 'R0') {
        memory[+cpu.IR.op2].op1 = cpu.R0;
      } else if (cpu.IR.op1 === 'R1') {
        memory[+cpu.IR.op2].op1 = cpu.R1;
      } else if (cpu.IR.op1 === 'R2') {
        memory[+cpu.IR.op2].op1 = cpu.R2;
      }
    } else if ((cpu.IR.op2 as string).includes('R') && memory[+cpu.IR.op1].name === 'DATA') {
      if (cpu.IR.op2 === 'R1') {
        cpu.R1 = memory[+cpu.IR.op1].op1;
      } else if (cpu.IR.op2 === 'R2') {
        cpu.R2 = memory[+cpu.IR.op1].op1;
      } else if (cpu.IR.op2 === 'R0') {
        cpu.R0 = memory[+cpu.IR.op1].op1;
      }
    } else {
      return {error: 'The instruction of the direction must be a data.'};
    }
    return {error: 'OK'};
  }
}

export class ADD extends Instruction {

  constructor() {
    super('ADD');
  }

  execute() {
    const cpu = CPU.getInstance();

    if (this.allAreRegisters()) {
      if ((this.op1 === 'R0' && this.op2 === 'R1' && this.res === 'R2') ||
        (this.op1 === 'R1' && this.op2 === 'R0' && this.res === 'R2')) {
        cpu.R2 = Number(cpu.R0) + Number(cpu.R1);
      } else if ((this.op1 === 'R0' && this.op2 === 'R2' && this.res === 'R1') ||
        (this.op1 === 'R2' && this.op2 === 'R0' && this.res === 'R1')) {
        cpu.R1 = Number(cpu.R0) + Number(cpu.R2);
      } else if ((this.op1 === 'R1' && this.op2 === 'R2' && this.res === 'R0') ||
        (this.op1 === 'R2' && this.op2 === 'R1' && this.res === 'R0')) {
        cpu.R0 = Number(cpu.R1) + Number(cpu.R2);
      }
      return {error: 'OK'};
    } else {
      return {error: 'All the operands must be registers.'};
    }
  }
}

export class COMPARE extends Instruction {

  constructor() {
    super('COMPARE');
  }

  execute() {
    const cpu = CPU.getInstance();
    const memory = Memory.getInstance().memory;

    let R0;
    let R1;

    if (this.bothAreRegisters()) {
      if (this.op1 === 'R0') {
        R0 = cpu.R0;
      } else if (this.op1 === 'R1') {
        R0 = cpu.R1;
      } else if (this.op1 === 'R2') {
        R0 = cpu.R2;
      }

      if (this.op2 === 'R0') {
        R1 = cpu.R0;
      } else if (this.op2 === 'R1') {
        R1 = cpu.R1;
      } else if (this.op2 === 'R2') {
        R1 = cpu.R2;
      }

      if (+R0 === +R1) {
        cpu.flag = true;
      } else {
        cpu.flag = false;
      }

    } else {
      return {error: 'Both must be registers'};
    }
    return {error: 'OK'};
  }

}

export class OUT extends Instruction {

  constructor() {
    super('OUT');
  }

  execute() {

    const cpu = CPU.getInstance();

    if (this.op1IsRegister() && this.op2 === 'PD') {
      cpu.PD = cpu[this.op1];
      return {error: 'OK'};
    } else if (!this.op1IsRegister())  {
      return {error: 'Source is not a register!'};
    } else {
      return {error: 'Destination is nor a valid port.'};
    }
  }

}

export class JUMP extends Instruction {

  constructor() {
    super('JUMP');
  }

  execute() {

    const cpu = CPU.getInstance();
    const memory = Memory.getInstance().memory;
    let index;

    memory.forEach(i => {
      if (i.tag === this.op1) {
        index = i.index;
      }
    });

    if (index) {
      cpu.PC = index;
      return {error: 'OK'};
    } else {
      return {error: 'Tag not found.'};
    }
  }

}

export class JNE extends Instruction {

  constructor() {
    super('JNE');
  }

  execute() {

    const cpu = CPU.getInstance();
    const memory = Memory.getInstance().memory;
    let index;
    if (!cpu.flag) {
      memory.forEach(i => {
        if (i.tag === this.op1) {
          index = i.index;
        }
      });

      if (index) {
        cpu.PC = index;
        return {error: 'OK'};
      } else {
        return {error: 'Tag not found.'};
      }
    } else {
      return {error: 'OK'};
    }
  }
}

export class JE extends Instruction {

  constructor() {
    super('JE');
  }

  execute() {

    const cpu = CPU.getInstance();
    const memory = Memory.getInstance().memory;
    let index;
    if (cpu.flag) {
      memory.forEach(i => {
        if (i.tag === this.op1) {
          index = i.index;
        }
      });
      if (index) {
        cpu.PC = index;
        return {error: 'OK'};
      } else {
        return {error: 'Tag not found.'};
      }
    } else {
      return {error: 'OK'};
    }
  }
}

export class DATA extends Instruction {

  constructor() {
    super('DATA');
  }

  execute() {
    return {error: 'All the data should be placed after the HALT instruction'};
  }
}

export class IN extends Instruction {

  constructor() {
    super('IN');
  }

  execute() {
    return {error: 'Error'};
  }
}

export class HALT extends Instruction {

  constructor() {
    super('HALT');
  }

  execute() {
    return {error: 'finished'};
  }
}

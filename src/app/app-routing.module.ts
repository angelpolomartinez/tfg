import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { BlocksComponent } from './blocks/blocks.component';
import { ExercisesListComponent } from './exercises-list/exercises-list.component';
import { RegisterComponent } from './register/register.component';
import { StudentsListComponent } from './students-list/students-list.component';
import { RankingComponent } from './ranking/ranking.component';
import { ExerciseFormComponent } from './exercise-form/exercise-form.component';
import {ProfileComponent} from './profile/profile.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'blocks/:exercise', component: BlocksComponent },
  { path: 'exercises/list', component: ExercisesListComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'students', component: StudentsListComponent },
  { path: 'ranking', component: RankingComponent },
  { path: 'exerciseForm', component: ExerciseFormComponent },
  { path: 'profile', component: ProfileComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import {Component, OnInit} from '@angular/core';
import {StudentService} from '../services/student.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss']
})
export class StudentsListComponent implements OnInit {

  private pending = [];
  private accepted = [];
  private declined = [];

  constructor(private studentsService: StudentService, private router: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('role') !== 'Admin') {
      this.router.navigateByUrl('/login');
    }
    this.studentsService.list().subscribe(list => {
      this.pending = list['pending'];
      this.accepted = list['accepted'];
      this.declined = list['declined'];
    });
  }

  editPermission(email, accepted) {
    this.studentsService.editAccepted(accepted, email.email).subscribe(result => {
      email.accepted = accepted;
    });
  }

  acceptAll() {
    this.studentsService.acceptAll().subscribe(result => {
      this.pending.forEach(student => {
        student.accepted = true;
      });
    });

  }

  blockAll() {
    this.studentsService.blockAll().subscribe(result => {
      this.pending.forEach(student => {
        student.accepted = false;
      });
    });
  }

  blockAllAccepted() {
    this.studentsService.blockAllAccepted().subscribe(result => {
      this.pending.forEach(student => {
        student.accepted = false;
      });
    });
  }

}

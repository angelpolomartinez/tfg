import {Component, Inject, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem} from '@angular/cdk/drag-drop';
import {Instruction, MOVE, ADD, COMPARE, OUT, JUMP, JE, JNE, HALT, IN, DATA} from '../models/instruction.model';
import {faPlay, faStepForward, faStepBackward, faStop, faMicrochip, faTrash, faInfoCircle} from '@fortawesome/free-solid-svg-icons';
import {CPU} from '../models/cpu';
import {AuthService} from '../services/auth.service';
import {Memory} from '../models/memory';
import {SnackBarService} from '../services/snack-bar.service';
import {ActivatedRoute} from '@angular/router';
import {ExercisesService} from '../services/exercises.service';
import {ResolutionService} from '../services/resolution.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {StudentService} from '../services/student.service';

export interface DialogData {
  points: number;
}

@Component({
  selector: 'app-blocks',
  templateUrl: './blocks.component.html',
  styleUrls: ['./blocks.component.scss']
})
export class BlocksComponent implements OnInit {

  /**
   * Instructions array
   */
  instructions: Instruction[];

  /**
   * Memory array
   */
  memory = Memory.getInstance();

  /**
   * Registers variables
   */
  /*R0: number = undefined;
  R1: number = undefined;
  R2: number = undefined;
  PC = 0;
  IR: Instruction;
  PD: number;
  PE: number;*/

  cpu: CPU = CPU.getInstance();

  /**
   * Check if the instructions are being executed
   */
  executing = false;

  /**
   * Exercise
   */

  exercise;

  /**
   * Icons variables
   */
  faPlay = faPlay;
  faStepForward = faStepForward;
  faStepBackward = faStepBackward;
  faStop = faStop;
  faMicrochip = faMicrochip;
  faTrash = faTrash;
  faInfo = faInfoCircle;

  constructor(private authService: AuthService,
              private snackBarService: SnackBarService,
              private route: ActivatedRoute,
              private exercisesService: ExercisesService,
              private resolutionService: ResolutionService,
              private studentService: StudentService,
              public dialog: MatDialog) {
    this.cpu.reset();
    this.memory.reset();
    this.restartInstructions();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('exercise');
    this.exercisesService.getExercise(+id).subscribe(data => {
      this.exercise = data;
      this.getResolution();
    });

  }

  /**
   * Method to drag and drop instructions to the memory
   */
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      this.restartInstructions();
      this.memory.memory[event.currentIndex].index = event.currentIndex;
    }
    this.setIndexes();
  }

  /**
   * Set the index of every instruction in memory after another is being added
   */
  setIndexes() {
    for (let i = 0; i < this.memory.memory.length; i++) {
      this.memory.memory[i].index = i;
    }
  }

  restartInstructions() {
    this.instructions = [
      new MOVE(),
      new ADD(),
      new DATA(),
      new COMPARE(),
      new IN(),
      new OUT(),
      new JUMP(),
      new JE(),
      new JNE(),
      new HALT(),
    ];
  }

  startExecution() {
    this.cpu.R0 = undefined;
    this.cpu.R1 = undefined;
    this.cpu.R2 = undefined;
    this.executing = true;
  }

  stopExecution() {
    this.executing = false;
    this.cpu.IR = undefined;
    this.cpu.PC = 0;
  }

  updateCU() {
    this.cpu.IR = this.memory.memory[this.cpu.PC];
    this.cpu.PC++;
  }

  executeInstruction() {
    this.updateCU();
    const resultado = this.cpu.IR.execute();

    if (resultado.error === 'finished') {
      const resolved = this.checkResolution();
      if (resolved) {
        this.openDialog();
        this.saveResolution(false);
        this.studentService.addPoints(this.exercise.points, this.exercise.id, new Date().toLocaleDateString()).subscribe(data => {
          this.saveResolution(true);
        });
      }
      this.snackBarService.openSnackBar('Execution finished', 'Dissmiss', 5000);
      this.stopExecution();
    } else if (resultado.error !== 'OK') {
      this.snackBarService.openSnackBar(resultado.error, 'Dissmiss', 5000);
      this.stopExecution();
    }
  }

  private checkResolution() {

    const result = this.exercise.result;

    if ((result.R0 && result.R0 !== '') && result.R0 !== this.cpu.R0) {
      return false;
    }

    if ((result.R1 && result.R1 !== '') && result.R1 !== this.cpu.R1) {
      return false;
    }
    if ((result.R2 && result.R2 !== '') && +result.R2 !== +this.cpu.R2) {
      return false;
    }
    if (result.PD && result.PD !== this.cpu.PD) {
      return false;
    }
    if (result.PE && result.PE !== this.cpu.PE) {
      return false;
    }

    const instructionsInMemory = [];

    this.memory.memory.forEach(instr => {
      instructionsInMemory.push(instr.name);
    });

    let allInstructions = true;

    result.instructions.forEach(instr => {
      if (!instructionsInMemory.includes(instr)) {
        allInstructions = false;
      }
    });

    return allInstructions;
  }

  removeInstrucion(index: number) {
    this.memory.memory.splice(index, 1);
    this.setIndexes();
  }

  saveResolution(resolved?: boolean) {

    const resolution = {
      blocks: this.memory.memory,
      exercise: this.exercise.id,
      completed: resolved ? resolved : false,
    };

    this.resolutionService.newResolution(resolution).subscribe();

  }

  getResolution() {

    this.resolutionService.getResolution(this.exercise.id).subscribe(data => {
      const blocks = data['blocks'];

      blocks.forEach(block => {
        let instr;
        switch (block.name) {
          case 'MOVE':
            instr = new MOVE();
            break;
          case 'ADD':
            instr = new ADD();
            break;
          case 'DATA':
            instr = new DATA();
            break;
          case 'COMPARE':
            instr = new COMPARE();
            break;
          case  'IN':
            instr = new IN();
            break;
          case  'OUT':
            instr = new OUT();
            break;
          case  'JUMP':
            instr = new JUMP();
            break;
          case  'JE':
            instr = new JE();
            break;
          case  'JNE':
            instr = new JNE();
            break;
          case  'HALT':
            instr = new HALT();
            break;
        }

        instr.index = block.index;
        instr.tag = block.tag;
        instr.op1 = block.op1;
        instr.op2 = block.op2;
        instr.res = block.res;

        this.memory.memory.push(instr);
      });

    });

  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: {points: this.exercise.points}
    });

    dialogRef.afterClosed().subscribe();
  }

}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

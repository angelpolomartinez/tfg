import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ExercisesService} from '../services/exercises.service';
import {SnackBarService} from '../services/snack-bar.service';
import {ADD, COMPARE, DATA, HALT, IN, JE, JNE, JUMP, MOVE, OUT} from '../models/instruction.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-exercise-form',
  templateUrl: './exercise-form.component.html',
  styleUrls: ['./exercise-form.component.scss']
})
export class ExerciseFormComponent implements OnInit {

  form: FormGroup;

  MOVE = false;
  ADD = false;
  DATA = false;
  COMPARE = false;
  IN = false;
  OUT = false;
  JUMP = false;
  JE = false;
  JNE = false;
  HALT = false;


  constructor(private exercisesService: ExercisesService,
              private snackBarService: SnackBarService,
              private fb: FormBuilder,
              private router: Router) {
    this.form = this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      difficulty: ['', [Validators.min(1), Validators.max(3), Validators.required]],
      points: ['', [Validators.required, Validators.min(1), Validators.max(100)]],
      R0: [''],
      R1: [''],
      R2: [''],
      PD: [''],
      PE: [''],
    });
  }

  ngOnInit() {
    if (localStorage.getItem('role') !== 'Admin') {
      this.router.navigateByUrl('/login');
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  newExercise() {
    const form = this.form.value;

    const instructions = [];

    if (this.MOVE) {
      instructions.push('MOVE');
    }
    if (this.ADD) {
      instructions.push('ADD');
    }
    if (this.DATA) {
      instructions.push('DATA');
    }
    if (this.COMPARE) {
      instructions.push('COMPARE');
    }
    if (this.IN) {
      instructions.push('IN');
    }
    if (this.OUT) {
      instructions.push('OUT');
    }
    if (this.JUMP) {
      instructions.push('JUMP');
    }
    if (this.JE) {
      instructions.push('JE');
    }
    if (this.JNE) {
      instructions.push('JNE');
    }
    if (this.HALT) {
      instructions.push('HALT');
    }

    const result = {
      title: form.title,
      description: form.description,
      difficulty: form.difficulty,
      points: form.points,
      result: {
        R0: form.R0,
        R1: form.R1,
        R2: form.R2,
        instructions
      }
    };


    this.snackBarService.openSnackBar('Exercise saved', 'Dismiss', 5000);

    this.exercisesService.newExercise(result).subscribe(data => {
      this.router.navigateByUrl('/exercises/list');
    });

  }

}

import {Component, OnInit} from '@angular/core';
import {ExercisesService} from '../services/exercises.service';
import {Router} from '@angular/router';
import {ResolutionService} from '../services/resolution.service';

@Component({
  selector: 'app-exercises-list',
  templateUrl: './exercises-list.component.html',
  styleUrls: ['./exercises-list.component.scss']
})
export class ExercisesListComponent implements OnInit {

  public exercises;
  private resolutions = {};

  constructor(private exercisesService: ExercisesService, private router: Router, private resolutionService: ResolutionService) {
  }

  ngOnInit() {
    this.exercisesService.list().subscribe(list => {
      this.exercises = list;
    });

    this.resolutionService.getResolutions().subscribe((list: []) => {
      list.forEach((res: any) => {
        this.resolutions[res.exercise] = res.completed;
      });
    });

  }

  counter(i: number) {
    return new Array(i);
  }

  deleteExercise(id: number, index: number) {
    this.exercises.splice(index, 1);
    this.exercisesService.removeExercise(id);
  }

  canDeleteExercise(): boolean {
    return localStorage.getItem('role') === 'Admin';
  }

  redirectToExercise(id: number) {
    this.router.navigateByUrl('blocks/' + id);
  }

}

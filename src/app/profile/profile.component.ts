import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ExercisesService} from '../services/exercises.service';
import {SnackBarService} from '../services/snack-bar.service';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  form: FormGroup;

  name: string;
  email: string;
  points: number;

  constructor(private userService: AuthService,
              private snackBarService: SnackBarService,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.userService.getUser().subscribe((data: any) => {
      this.name = data.name;
      this.points = data.points;
      this.email = data.username;
    });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  changeName() {
    this.userService.changeName(this.name).subscribe((data: any) => {
      if (data.OK) {
        localStorage.setItem('name', this.name);
        this.snackBarService.openSnackBar('Name changed to ' + this.form.value.name, 'Dismiss', 5000);
      }
    });

  }
}

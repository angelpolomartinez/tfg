import { Component, OnInit } from '@angular/core';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {

  public list = [];

  constructor(private studentService: StudentService) { }

  displayedColumns: string[] = ['position', 'name', 'points', 'date'];

  ngOnInit() {
    this.studentService.ranking().subscribe(ranking => {
      this.list = ranking['list'];
    });
  }

}

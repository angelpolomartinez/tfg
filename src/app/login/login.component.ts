import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {SnackBarService} from '../services/snack-bar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService, private snackBarService: SnackBarService,) {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    const val = this.form.value;

    if (val.email && val.password) {
      this.authService.login(val.email, val.password).subscribe((token: any) => {
        localStorage.setItem('token', token.token);
        localStorage.setItem('role', token.role);
        localStorage.setItem('name', token.name);
        this.router.navigateByUrl('exercises/list');
      }, error => {
        console.log(error);
        if (error.status === 401) {
          this.snackBarService.openSnackBar('Wrong credentials', 'Close', 5000);
        } else {
          this.snackBarService.openSnackBar('You are blocked or not accepted in the platform', 'Close', 5000);
        }
      });
    }
  }

  redirectToRegister() {
    this.router.navigateByUrl('/register');
  }

  ngOnInit() {
  }

}

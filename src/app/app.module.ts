import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {BlocksComponent, DialogOverviewExampleDialog} from './blocks/blocks.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatInputModule} from '@angular/material/input';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {MatTabsModule} from '@angular/material/tabs';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {AppRoutingModule} from './app-routing.module';
import {LoginComponent} from './login/login.component';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule, MatDialogModule, MatSnackBarModule} from '@angular/material';
import {MatChipsModule} from '@angular/material';

// Interceptors
import {AuthInterceptorService} from './services/auth-interceptor.service';
import {ExercisesListComponent} from './exercises-list/exercises-list.component';
import {ExercisesComponent} from './exercises/exercises.component';
import {RegisterComponent} from './register/register.component';
import {StudentsListComponent} from './students-list/students-list.component';
import {RankingComponent} from './ranking/ranking.component';
import {ExerciseFormComponent} from './exercise-form/exercise-form.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    BlocksComponent,
    LoginComponent,
    ExercisesListComponent,
    ExercisesComponent,
    RegisterComponent,
    StudentsListComponent,
    RankingComponent,
    ExerciseFormComponent,
    DialogOverviewExampleDialog,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FontAwesomeModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatToolbarModule,
    FlexLayoutModule,
    ScrollingModule,
    MatTooltipModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    MatTabsModule,
    MatTableModule,
    MatSnackBarModule,
    MatChipsModule,
    MatCheckboxModule,
    MatDialogModule,
  ],
  entryComponents: [BlocksComponent, DialogOverviewExampleDialog],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

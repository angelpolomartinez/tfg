import {Component} from '@angular/core';
import {AuthService} from './services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'easyassembly';

  constructor(private authService: AuthService, private router: Router) {

  }

  logOut() {
    this.authService.logout();
  }

  redirectToExercises() {
    this.router.navigateByUrl('exercises/list');
  }

  redirectToStudents() {
    this.router.navigateByUrl('students');
  }

  redirectToRanking() {
    this.router.navigateByUrl('ranking');
  }

  redirectToExerciseForm() {
    this.router.navigateByUrl('exerciseForm');
  }

  redirectToProfile() {
    this.router.navigateByUrl('profile');
  }

  isAdmin(): boolean {
    return localStorage.getItem('role') === 'Admin';
  }

  getName(): string {
    return localStorage.getItem('name');
  }

  showNavBar(): boolean {
    return this.router.url !== '/login' && this.router.url !== '/' && this.router.url !== '/register';
  }


}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResolutionService {

  constructor(private http: HttpClient) {
  }

  newResolution(resolution) {
    return this.http.post(environment.apiUrl + '/api/resolution', {
      exercise: resolution.exercise,
      blocks: resolution.blocks,
      completed: resolution.completed,
    });
  }

  getResolution(id) {
    return this.http.get(environment.apiUrl + '/api/resolution/' + id);
  }

  getResolutions() {
    return this.http.get(environment.apiUrl + '/api/resolutions/');
  }
}

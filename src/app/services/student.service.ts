import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) {
  }

  list() {
    return this.http.get(environment.apiUrl + '/api/users/list');
  }

  editAccepted(accepted: boolean, email: string) {
    return this.http.put(environment.apiUrl + '/api/users/edit', {email, accepted});
  }

  ranking() {
    return this.http.get(environment.apiUrl + '/api/users/ranking');
  }

  acceptAll() {
    return this.http.get(environment.apiUrl + '/api/users/acceptAll');
  }

  blockAll() {
    return this.http.get(environment.apiUrl + '/api/users/blockAll');
  }

  blockAllAccepted() {
    return this.http.get(environment.apiUrl + '/api/users/blockAllAccepted');
  }

  addPoints(points: number, exercise: number, date: string) {
    return this.http.put(environment.apiUrl + '/api/users/points', {points, exercise, date});
  }

}

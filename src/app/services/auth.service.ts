import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(private http: HttpClient, private router: Router) {
  }

  login(email: string, password: string) {
    const user: string = email + ':' + password;
    const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(user)});

    return this.http.get(environment.apiUrl + '/api/token', {headers});
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    this.router.navigateByUrl('/login');
  }

  signUp(email, password, name) {
    this.http.post(environment.apiUrl + '/api/user', {
      username: email,
      password,
      name,
      professor: false
    }).subscribe();
  }

  getUser() {
    return this.http.get(environment.apiUrl + '/api/users/data');
  }

  changeName(name: string) {
    return this.http.put(environment.apiUrl + '/api/users/name', {name});
  }
}

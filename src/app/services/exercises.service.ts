import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExercisesService {

  constructor(private http: HttpClient) {
  }

  list() {
    return this.http.get(environment.apiUrl + '/api/exercises');
  }

  getExercise(id: number) {
    return this.http.get(environment.apiUrl + '/api/exercise/' + id);
  }

  newExercise(exercise) {
    return this.http.post(environment.apiUrl + '/api/exercise', {
      title: exercise.title,
      description: exercise.description,
      difficulty: exercise.difficulty,
      points: exercise.points,
      result: JSON.stringify(exercise.result)
    });
  }

  removeExercise(exerciseID: number) {
    return this.http.delete(environment.apiUrl + '/api/exercises/' + exerciseID).subscribe();
  }

}

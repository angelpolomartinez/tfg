export const environment = {
  production: true,
  apiUrl: 'https://easy-assembly-api.herokuapp.com'
};
